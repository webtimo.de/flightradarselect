import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {WindowComponent} from './window/window.component';
import {HttpClientModule} from "@angular/common/http";
import { BoxComponent } from './box/box.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule} from "@angular/forms";
import { ViewComponent } from './view/view.component';

@NgModule({
    declarations: [
        AppComponent,
        WindowComponent,
        BoxComponent,
        ViewComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
