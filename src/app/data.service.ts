import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

export interface RequestOption {
    title?: string;
    url?: string;
    subtitle?: string | null;
    color?: string | null;
}

@Injectable({
    providedIn: 'root'
})
export class DataService {


    public config: RequestOption[] = [
        {
            title: "Radar 01",
            color: null,
            subtitle: "JetVision Radarcape (400km)",
            url: "https://flightradar.web-timo.de/radar01/"
        },
        {
            title: "Radar 02",
            color: null,
            subtitle: "Raspberry Pi (max. 50km)",
            url: "https://flightradar.web-timo.de/radar02/"
        }
    ];


    constructor(@Inject(HttpClient) private http: HttpClient) {
    }

    // public getOption(): Promise<any> {
    //
    // }

}
