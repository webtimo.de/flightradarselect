import {Component, OnInit} from '@angular/core';
import {DataService} from "../data.service";

@Component({
    selector: 'app-box',
    templateUrl: './box.component.html',
    styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {

    constructor(public data: DataService) {
    }

    ngOnInit(): void {
    }

}
