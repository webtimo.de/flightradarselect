import {Component, OnInit} from '@angular/core';
import {DataService} from "../data.service";

@Component({
    selector: 'app-window',
    templateUrl: './window.component.html',
    styleUrls: ['./window.component.scss']
})
export class WindowComponent implements OnInit {

    constructor(public data: DataService) {
    }

    ngOnInit(): void {
    }

    linkTo(s: string) {
        window.location.href = s;
    }
}
